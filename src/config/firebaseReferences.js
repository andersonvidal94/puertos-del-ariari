import { storage, firestore } from '@/config/firebase'

export const usersRef = firestore.collection('users')
export const articlesRef = firestore.collection('articles')

export const storageIMG = storage.ref('test')
