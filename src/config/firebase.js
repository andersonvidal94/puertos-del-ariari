import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/storage'
import 'firebase/firestore'

firebase.initializeApp({
    apiKey: 'AIzaSyCjMKq_1e3-iwjsi1oIcfra3-F3mQ2hKOo',
    authDomain: 'cnape-3a983.firebaseapp.com',
    databaseURL: 'https://cnape-3a983.firebaseio.com',
    projectId: 'cnape-3a983',
    storageBucket: 'cnape-3a983.appspot.com',
    messagingSenderId: '573742889151'
})

const auth = firebase.auth()
const storage = firebase.storage()
const firestore = firebase.firestore()

export { auth, storage, firestore, firebase }
