import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import { auth } from '@/config/firebase'
import { usersRef } from '@/config/firebaseReferences'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@/plugins/swiper'
import '@/plugins/veeValidate'
import '@/plugins/VueNotification'
import '@/plugins/vuetify'

Vue.config.productionTip = false

async function initApp(currentUser) {
    const usersResp = await usersRef.doc(currentUser.uid).get()
    store.dispatch('initApp', { id: currentUser.uid, ...usersResp.data() })
    initVue()
}

function initVue() {
    new Vue({
        router,
        store,
        render: (h) => h(App)
    }).$mount('#app')
}

auth.onAuthStateChanged((currentUser) => {
    if (currentUser !== null) {
        initApp(currentUser)
    } else {
        initVue()
    }
})
