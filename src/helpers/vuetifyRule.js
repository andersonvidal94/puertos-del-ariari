export function vuetifyRule(required, min, max) {
    const rules = []

    if (required) {
        const rule = v => !!v || required
        rules.push(rule)
    }

    if (min) {
        const rule = v => (v || "").length >= min || `Se permite un mínimo de ${min} caracteres.`
        rules.push(rule)
    }

    if (max) {
        const rule = v => (v || "").length <= max || `Se permite un máximo de ${max} caracteres.`
        rules.push(rule)
    }

    return rules
}
