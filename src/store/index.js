import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import auth from './modules/auth'
import request from './modules/request'
import snackbar from './modules/snackbar'
import storage from './modules/storage'

const store = new Vuex.Store({
    modules: { auth, request, snackbar, storage },
    state: {},
    getters: {},
    mutations: {},
    actions: {
        initApp({ commit, dispatch }, currentUser) {
            commit('userMutation', currentUser)
        }
    }
})

export default store
