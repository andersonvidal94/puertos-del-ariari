import {Article} from "../../../models/Article"
const state = {
    storageArticle: new Article(),
    storageArticles: {},
    storageCategory: {},
    storageCategories: [],
    storageCounter:{
        articleViews: 0,
        articleCount: 0,
        categories: {
            id:{count: 0, views: 0}
        }
    },
    storageLastArticleVisible: {},
}

const getters = {
    storageArticle: state => state.storageArticle,
    storageArticles: state => state.storageArticles,
    storageCategory: state => state.storageCategory,
    storageCategories: state => state.storageCategories,
    storageCounter: state => state.storageCounter,
    storageLastArticleVisible: state => state.storageLastArticleVisible,
}

const mutations = {
    storageArticleHandler(state, payload) {
        state.storageArticle = payload
    },
    storageArticlesHandler(state, payload) {
        const {parentId, data, isToAdd} = payload
        isToAdd
            ? state.storageArticles[parentId].push(data)
            : state.storageArticles[parentId] = data
    },
    storageCategoryHandler(state, payload) {
        state.storageCategory = payload
    },
    storageCategoriesHandler(state, payload) {
        state.storageCategories = payload
    },
    storageCounterHandler(state, payload) {
        state.storageCounter = payload
    },
    storageLastArticleVisibleHandler(state, payload) {
        const {parentId, data} = payload
        state.storageLastArticleVisible[parentId] = data
    },
   storageArticlesDelete(state, payload){
       const {id, parentId} = payload
       const found = state.storageArticles[parentId].findIndex(i=> i.id === id)
       state.storageArticles[parentId].splice(found,1)

   },
    storageArticlesEdit(state, payload){
        const {id, data, parentId} = payload
        const found = state.storageArticles[parentId].findIndex(i=> i.id === id)
        console.log('editing', found, "with", data)
        state.storageArticles[parentId][found] = data


    }
}

const actions = {}

export default {
    state,
    getters,
    mutations,
    actions
}
