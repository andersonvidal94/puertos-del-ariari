const state = {
    user: null
}

const getters = {
    user: state => state.user
}

const actions = {}

const mutations = {
    userMutation(state, payload) {
        state.user = payload
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
