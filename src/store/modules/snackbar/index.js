const state = {
    snackBar: {}
}

const getters = {
    snackBar: (state) => state.snackBar
}

const actions = {}

const mutations = {
    snackBarPush: (state, payload) => {
        state.snackBar = payload
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
