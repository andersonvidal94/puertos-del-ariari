import { firestore } from '@/config/firebase'
import router from '@/router'
import store from '@/store'

const state = {}

const getters = {}

const mutations = {}

const actions = {
    GET({ state, commit, dispatch }, payload) {
        return new Promise((resolve, reject) => {
        const { ref, storage, storageHandler, preserve, isMultiple, id } = payload
        const stateStorage = store.state.storage[storage]
            ref.get().then(snapshot => {
                let data = []
                if(isMultiple){
                    data = (preserve && stateStorage) ? stateStorage[id] || []: []
                }
                else
                data = (preserve && stateStorage) ? stateStorage : []
                if (snapshot.forEach) {
                    snapshot.forEach(doc => {
                        doc.exists &&
                        data.push({id: doc.id, ...doc.data()})
                    })
                    if(isMultiple)
                        data = {parentId: id, data: data}
                    commit(storageHandler, data)
                    resolve(snapshot.docs[snapshot.docs.length - 1])
                } else {
                    commit(storageHandler, {id: snapshot.id, ...snapshot.data()})
                    resolve(snapshot.ref)
                }
            })
        })

    },
    SAVE({ commit, dispatch }, payload) {
        const { ref, storage, storageHandler, route, data } = payload
        return new Promise((resolve, reject) => {
            commit(storageHandler, [])
            ref.set(data).then(() => {
                router.push(route)
                resolve('success')
            })
        })
    },
    GET_COUNTERS({ commit, dispatch }, payload){
        const date = new Date()
        const dateMonthAgo = date.setMonth(date.getMonth()-1)
        const { ref, storageHandler, storageState } = payload
        /*
        * Consulta la colleción de visitas y recorre los documentos para calcular el total de visitas
        * y el total por categoria, IMPORTANTE el where para evitar desborde de consultas
        * */
        ref
            .where("created_at", ">", new Date(dateMonthAgo) )
            .get()
            .then(snapshot => {
            let counter = storageState
            snapshot.forEach(doc => {
                if(doc.exists){                   
                    
                    const data = doc.data()
                    const parentCategoryId = doc.ref.parent.parent.id
                    counter.articleViews += data.views
                    counter.articleCount++
                    if (counter.categories.hasOwnProperty( parentCategoryId)) {
                        counter.categories[ parentCategoryId].views += data.views
                        counter.categories[ parentCategoryId].count++
                    } else
                        counter.categories[ parentCategoryId] = {count: 1, views: data.views}
                    commit(storageHandler, counter)
                }

            })
            
        })
    },
    DELETE({ commit, dispatch }, payload) {
        const {query, deleteHandler, id} = payload
        query.delete().then(()=>{
            commit(deleteHandler, id )
        })
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}
