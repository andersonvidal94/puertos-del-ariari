import Vue from 'vue'
import Vuetify from 'vuetify'
import es from 'vuetify/es5/locale/es'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify, {
    theme: {
        // primary: '#f40008'
        // secondary: '#424242',
        // accent: '#82B1FF',
        // error: '#FF5252',
        // info: '#2196F3',
        // success: '#4CAF50',
        // warning: '#FFC107'
    },
    lang: {
        locale: es
    },
    iconfont: 'mdi'
})
