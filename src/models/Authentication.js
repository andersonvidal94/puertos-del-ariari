export class Authentication {
    constructor(data) {
        const boolean = data !== undefined

        this.confirmation = boolean ? data.confirmation : ''
        this.created_at = boolean ? data.created_at : null
        this.email = boolean ? data.email : ''
        this.email_verified = boolean ? data.email_verified : false
        this.name = boolean ? data.name : ''
        this.password = boolean ? data.password : ''
        this.role = boolean ? data.role : 'admin'
        this.updated_at = boolean ? data.updated_at : null
    }
}

export const dictionary = {
    attributes: {
        confirmation: 'confirmar contraseña',
        email: 'correo electrónico',
        name: 'nombre completo',
        password: 'contraseña'
    }
}
