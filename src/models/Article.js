export class Article {
    constructor(data) {
        const boolean = data !== undefined
        this.id = boolean ? data.id : ''
        this.content = boolean ? data.content : []
        this.created_at = boolean ? data.created_at : null
        this.created_by = boolean ? data.created_by : ''
        this.description = boolean ? data.description : ''
        this.image = boolean ? data.image : ''
        this.image_url = boolean ? data.image_url : ''
        this.published = boolean ? data.published : false
        this.title = boolean ? data.title : ''
        this.updated_at = boolean ? data.updated_at : null
        this.updated_by = boolean ? data.updated_by : ''
    }
}

export const dictionary = {
    attributes: {
        title: 'título',
        description: 'descripción',
        imgPath: 'imagen'
    }
}
