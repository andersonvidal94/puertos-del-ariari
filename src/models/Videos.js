class Videos {
    constructor(data) {
        const boolean = data !== undefined

        this.createdAt = boolean ? data.createdAt : ""
        this.description = boolean ? data.description : ""
        this.title = boolean ? data.title : ""
        this.updatedAt = boolean ? data.updatedAt : ""
        this.youtube = boolean ? data.youtube : ""
    }
}

export default Videos
