export class Categories {
    constructor(data) {
        const boolean = data !== undefined
        this.created_at = boolean ? data.created_at : new Date()
        this.description = boolean ? data.description : ""
        this.title = boolean ? data.title : ""
        this.updated_at = new Date()
    }
}

export const dictionary = {
    attributes: {
        title: "nombre",
        description: "descripción"
    }
}
