const backend = {
    path: '/admin',
    component: () => import('@/layouts/backend/index.vue'),
    meta: {
        requireAuth: true
    },
    children: [
        {
            path: '',
            component: () => import('@/views/backend/Home.vue'),
            meta: {
                title: 'Panel de control'
            }
        },
        {
            path: 'articulos/:id?',
            component: () => import('@/views/backend/articles/index.vue'),
            meta: {
                title: 'Categorías'
            }
        },
        {
            path: 'categories/:id?',
            component: () => import('@/views/backend/Categories.vue'),
            meta: {
                title: 'Categorías'
            }
        },
        {
            path: 'polls/:id?',
            component: () => import('@/views/backend/Polls.vue'),
            meta: {
                title: 'Encuestas'
            }
        },
        {
            path: 'settings/:id?',
            component: () => import('@/views/backend/Settings.vue'),
            meta: {
                title: 'Configuración'
            }
        },
        {
            path: 'videos/:id?',
            component: () => import('@/views/backend/Videos.vue'),
            meta: {
                title: 'Videos'
            }
        },
        {
            path: 'test/:id?',
            component: () => import('@/views/backend/Test.vue'),
            meta: {
                title: 'Pruebas'
            }
        }
    ]
}

export default backend
