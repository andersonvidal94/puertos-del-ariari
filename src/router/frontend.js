const frontend = {
    path: '/',
    component: () => import('@/layouts/frontend/index.vue'),
    meta: {
        title: 'Este es el home'
    },
    children: [
        {
            path: '',
            component: () => import('@/views/frontend/Home.vue')
        },
        {
            path: 'categoria/:id?',
            component: () => import('@/views/frontend/Category.vue')
        },
        {
            path: 'articulo/:id?',
            component: () => import('@/views/frontend/Article.vue')
        }
    ]
}

export default frontend
