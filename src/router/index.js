import Vue from 'vue'
import Router from 'vue-router'

import store from '../store'

import routesFrontend from './frontend'
import routesBackend from './backend'
import routesAuth from './auth'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [routesFrontend, routesBackend, routesAuth]
})

router.beforeEach((to, from, next) => {
    const auth = to.matched.some((record) => record.meta.requireAuth)
    const guest = to.matched.some((record) => record.meta.requireGuest)

    const currentUser = store.getters.user

    if (auth) {
        if (currentUser !== null) {
            if (currentUser.role === 'admin') {
                next()
            }
        } else {
            next('/autenticacion')
        }
    } else if (guest) {
        if (currentUser !== null) {
            next('/admin')
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router
