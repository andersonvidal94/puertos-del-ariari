const auth = {
    path: "/autenticacion",
    component: () => import("@/layouts/auth/index.vue"),
    meta: {
        requireGuest: true
    },
    children: [
        {
            path: "",
            component: () => import("@/views/auth/Home.vue")
        }
    ]
}

export default auth
