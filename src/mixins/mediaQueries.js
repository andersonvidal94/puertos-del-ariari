const mixin = {
    data() {
        return {
            mqXS: false,
            mqSM: false,
            mqMD: false,
            mqLG: false,
            mqXL: false
        }
    },
    methods: {
        mqObs() {
            const mqXS = window.matchMedia('(max-width: 576px)')
            const mqSM = window.matchMedia('(max-width: 768px)')
            const mqMD = window.matchMedia('(max-width: 992px)')
            const mqLG = window.matchMedia('(max-width: 1280px)')

            const mqCheck = () => {
                if (mqXS.matches) {
                    this.mqXS = true
                    this.mqSM = false
                    this.mqMD = false
                    this.mqLG = false
                    this.mqXL = false
                } else if (mqSM.matches) {
                    this.mqXS = false
                    this.mqSM = true
                    this.mqMD = false
                    this.mqLG = false
                    this.mqXL = false
                } else if (mqMD.matches) {
                    this.mqXS = false
                    this.mqSM = false
                    this.mqMD = true
                    this.mqLG = false
                    this.mqXL = false
                } else if (mqLG.matches) {
                    this.mqXS = false
                    this.mqSM = false
                    this.mqMD = false
                    this.mqLG = true
                    this.mqXL = false
                } else {
                    this.mqXS = false
                    this.mqSM = false
                    this.mqMD = false
                    this.mqLG = false
                    this.mqXL = true
                }
            }

            mqCheck()

            window.addEventListener('resize', () => {
                mqCheck()
            })
        },
        mqReturn(xs, sm, md, lg, xl) {
            let value = null
            if (this.mqXS) {
                value = xs
            } else if (this.mqSM) {
                value = sm
            } else if (this.mqMD) {
                value = md
            } else if (this.mqLG) {
                value = lg
            } else if (this.mqXL) {
                value = xl
            }

            return value
        }
    },
    created() {
        this.mqObs()
    }
}

export default mixin
