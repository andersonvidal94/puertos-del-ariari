const mixin = {
    data() {
        return {
            dateString: ''
        }
    },
    methods: {
        formatDateToString() {
            const date = new Date()
            const year = date.getFullYear()
            const month = date.getMonth()
            const day = date.getDay()
            const numberDay = date.getDate()

            const monthNames = [
                'Enero',
                'Febrero',
                'Marzo',
                'Abril',
                'Mayo',
                'Junio',
                'Julio',
                'Agosto',
                'Septiembre',
                'Octubre',
                'Noviembre',
                'Diciembre'
            ]

            const dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']

            this.dateString = `${dayNames[day]}, ${numberDay} de ${monthNames[month]} de ${year}`
        }
    },
    created() {
        this.formatDateToString()
    }
}

export default mixin
